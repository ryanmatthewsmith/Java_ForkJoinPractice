package hasOver;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class HasOver {
	private static int CUTOFF;
	private static int VALUE;
    public static boolean hasOver(int val, int[] arr, int sequentialCutoff) {
    	 CUTOFF = sequentialCutoff;
    	 VALUE = val;
    	 return has(arr);
    }
    
    public static boolean seqHasOver(int[] arr, int lo, int hi){
    	for (int i = lo; i<hi; i++){
    		if(arr[i]> VALUE){
    			return true;
    		}
    	}
    	return false;
    }
    
    static class HasOverTask extends RecursiveTask<Boolean>{
        int[] arr;
        int lo;
        int hi;
        public HasOverTask(int[] arr, int lo, int hi){
        	this.arr = arr;
        	this.lo = lo;
        	this.hi = hi;
        }
        public Boolean compute(){
        	if(hi - lo <= CUTOFF){
        		return (seqHasOver(this.arr, lo, hi));
        	}
        	
      	    int mid = lo + (hi - lo) / 2;
      	    
      	    HasOverTask left = new HasOverTask(arr, lo, mid);
      	    HasOverTask right = new HasOverTask(arr, mid, hi);
      	    
      	    right.fork();
      	    
      	    boolean leftBool = left.compute();
      	    boolean rightBool = right.join();
      	    
      	    return leftBool || rightBool;
        }
      
        
      }
      private static ForkJoinPool POOL = new ForkJoinPool();
      
      public static boolean has(int[] arr){
    	  return POOL.invoke(new HasOverTask(arr, 0, arr.length));
      }
    
    private static void usage() {
        System.err.println("USAGE: HasOver <number> <array> <sequential cutoff>");
        System.exit(2);
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            usage();
        }

        int val = 0;
        int[] arr = null;

        try {
            val = Integer.parseInt(args[0]); 
            String[] stringArr = args[1].replaceAll("\\s*",  "").split(",");
            arr = new int[stringArr.length];
            for (int i = 0; i < stringArr.length; i++) {
                arr[i] = Integer.parseInt(stringArr[i]);
            }
            System.out.println(hasOver(val, arr, Integer.parseInt(args[2])));
        } catch (NumberFormatException e) {
            usage();
        }
        
    }
    
}
