package filterEmpty;

import java.util.Arrays;
import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveAction;

public class FilterEmpty {
    static ForkJoinPool POOL = new ForkJoinPool();
    public static int[] bitset;

    public static int[] filterEmpty(String[] arr) {
        bitset = new int[arr.length];       

        mapToBitTask useBit = new mapToBitTask(arr, 0, arr.length);
        POOL.invoke(useBit);

        int[] bitsum = ParallelPrefixSum.parallelPrefixSum(bitset);
        if(bitsum.length == 0)return new int [0];
        
        int[] output = new int[bitsum[bitsum.length-1]];
        mapToOutputTask useOut = new mapToOutputTask(arr, bitsum, output, 0, arr.length);
        POOL.invoke(useOut);

        return output;
        

    }
    
    static class mapToBitTask extends RecursiveAction{
       String[] arr;
       int lo, hi;
       mapToBitTask(String[] arr, int lo, int hi){
    	   this.arr = arr;
    	   this.lo = lo;
    	   this.hi = hi;
       }
       public void compute(){
    	   if(hi-lo <= 1){
    		   mapToBitSet(arr, lo, hi);
    	   }else{
    		   int mid = lo + (hi-lo)/2;
    		   mapToBitTask left = new mapToBitTask(arr, lo, mid);
    		   mapToBitTask right = new mapToBitTask(arr, mid, hi);
    		   left.fork();
    		   right.compute();
    		   left.join();
    	   }
       }
    }

    public static void mapToBitSet(String[] arr,int lo,int hi) {
    	for (int i = lo; i<hi ; i++){
        	if(arr[i].length()==0){
        		bitset[i] = 0;
        	} else{
        		bitset[i] = 1;
        	}
    	}
    }
    

    
    public static void mapToOutput(String[] input, int[] bitsum, int[] output, int lo, int hi) {

        for(int i = lo; i<hi; i++){
        	if(bitset[i] == 1){
        		output[bitsum[i] -1] = input[i].length();
        	}
        }
    }
    
    static class mapToOutputTask extends RecursiveAction{
        String[] arr;
        int[]bitsum;
        int[]output;
        int lo, hi;
        mapToOutputTask(String[] arr,int[] bitsum, int[] output, int lo, int hi){
     	   this.arr = arr;
     	   this.bitsum = bitsum;
     	   this.output = output;
     	   this.lo = lo;
     	   this.hi = hi;
        }
        public void compute(){
     	   if(hi-lo <= 1){
     		   mapToOutput(arr, bitsum, output, lo, hi);
     	   }else{
     		   int mid = lo + (hi-lo)/2;
     		   mapToOutputTask left = new mapToOutputTask(arr, bitsum, output, lo, mid);
     		   mapToOutputTask right = new mapToOutputTask(arr, bitsum, output, mid, hi);
     		   left.fork();
     		   right.compute();
     		   left.join();
     	   }
        }
     }

    private static void usage() {
        System.err.println("USAGE: FilterEmpty <String array>");
        System.exit(1);
    }

    public static void main(String[] args) {
        if (args.length != 1) {
            usage();
        }

        String[] arr = args[0].replaceAll("\\s*", "").split(",");
        System.out.println(Arrays.toString(filterEmpty(arr)));
    }
}