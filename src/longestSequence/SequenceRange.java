package longestSequence;

public class SequenceRange {
    public int matchingOnLeft, matchingOnRight;
    public int longestRange;

    public SequenceRange(int left, int right, int longest) {
        this.matchingOnLeft = left;
        this.matchingOnRight = right;
        this.longestRange = longest;
    }
    
    
}
