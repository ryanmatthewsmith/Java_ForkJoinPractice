package longestSequence;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;


public class LongestSequence {
	private static int CUTOFF;
	private static int VALUE;
    public static int getLongestSequence(int val, int[] arr, int sequentialCutoff) {
        CUTOFF = sequentialCutoff;
        VALUE = val;
        SequenceRange mySeq = forkLongestTask(arr);
        return mySeq.longestRange;
    }
    public static SequenceRange seqLongest(int[] arr, int lo, int hi){

    	int max = 0;
    	int left = 0;
    	int right = 0;
    	int index = lo;
    	
    	while (index < hi){
    		if(index == lo){
    	      while(arr[index]==VALUE){
    	    	left++;
    	    	right++;
    	    	max++;
    	    	index++;  
    	    	if (index == hi)break;
    	      }
    		}
    	    if (index == hi)break;
    	    if(arr[index]==VALUE){
    	    	right++;
    	    	if (right > max)max = right;
    	    } else{
    	    	right = 0;
    	    }
    	    index++;
    	}
        
    	SequenceRange curr = new SequenceRange(left,right,max);
    	
    	return curr;
    }
    static class longestTask extends RecursiveTask<SequenceRange>{
    	int[] arr;
        int lo;
        int hi;
       
        public longestTask(int[] arr, int lo, int hi){
        	this.arr = arr;
        	this.lo = lo;
        	this.hi = hi;
        }	
        
        public SequenceRange compute(){
        	if(hi-lo <= CUTOFF){
        		return (seqLongest(arr, lo, hi));
        	}
        	
        	int mid = lo + (hi-lo)/2;

        	longestTask left = new longestTask(arr, lo, mid);
        	longestTask right = new longestTask(arr, mid, hi);
        	
        	right.fork();
        	SequenceRange leftLong = left.compute();
        	SequenceRange rightLong = right.join();
        	
        	int newLong;
        	int midLen;
        	int leftEdge = leftLong.matchingOnLeft;
        	int rightEdge = rightLong.matchingOnRight;
        	newLong = Math.max(rightLong.longestRange,leftLong.longestRange);
            midLen = leftLong.matchingOnRight+rightLong.matchingOnLeft;
            
            
            //edge combining in place of using seq var of Sequence Range
            if(leftEdge == mid-lo){
            	leftEdge = midLen;
            }
            if(rightEdge == hi-mid){
            	rightEdge = midLen;
            }
        	        	
        	SequenceRange ret = new SequenceRange(leftEdge,rightEdge,Math.max(midLen, newLong));
        	
        	return ret;
        	
        }
    	
    }
    private static ForkJoinPool POOL = new ForkJoinPool();
    
    public static SequenceRange forkLongestTask(int[] arr){
    	return POOL.invoke(new longestTask(arr, 0, arr.length));
    }
    
    private static void usage() {
        System.err.println("USAGE: LongestSequence <number> <array> <sequential cutoff>");
        System.exit(2);
    }

    public static void main(String[] args) {
    	test();
        if (args.length != 3) {
            usage();
        }

        int val = 0;
        int[] arr = null;

        try {
            val = Integer.parseInt(args[0]); 
            String[] stringArr = args[1].replaceAll("\\s*",  "").split(",");
            arr = new int[stringArr.length];
            for (int i = 0; i < stringArr.length; i++) {
                arr[i] = Integer.parseInt(stringArr[i]);
            }
            System.out.println(getLongestSequence(val, arr, Integer.parseInt(args[2])));
        } catch (NumberFormatException e) {
            usage();
        }
    }
    public static void test(){
    	int[] myarr = {8,8,8,1,1,8,2,5,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,8,4,3,2,8,10,12,8,8,128,8,8,8,8,8,8,8,8,8,8,8,8};
    	int cut = 12;
    	int num = 8;
    	System.out.println(getLongestSequence(num,myarr,cut));
    }
}










//package longestSequence;
//import java.util.concurrent.ForkJoinPool;
//import java.util.concurrent.RecursiveTask;
//public class LongestSequence {
//	private static int CUTOFF;
//	private static int VALUE;
//    public static int getLongestSequence(int val, int[] arr, int sequentialCutoff) {
//        CUTOFF = sequentialCutoff;
//        VALUE = val;
//        return forkLongestTask(arr);
//    }
//    public static int seqLongest(int[] arr){
//    	int max = 0;
//    	int curr = 0;
//    	for(int i=0; i<arr.length; i++){
//    		if (arr[i]==VALUE){
//    			curr++;
//    		} else{
//    			curr = 0;
//    		}
//    		if (curr > max){
//    			max = curr;
//    		}
//    	}
//    	return max;
//    }
//    static class longestTask extends RecursiveTask<Integer>{
//    	int[] arr;
//        int lo;
//        int hi;
//        public longestTask(int[] arr, int lo, int hi){
//        	this.arr = arr;
//        	this.lo = lo;
//        	this.hi = hi;
//        }	
//        
//        public Integer compute(){
//        	if(hi-lo <= CUTOFF){
//        		return (seqLongest(this.arr));
//        	}
//        	
//        	int mid = lo + (hi-lo)/2;
//        	while(arr[mid]==VALUE && mid != lo+1){
//        		mid--;
//        	}
//        	longestTask left = new longestTask(arr,lo,mid);
//        	longestTask right = new longestTask(arr, mid, hi);
//        	
//        	right.fork();
//        	int leftLong = left.compute();
//        	int rightLong = right.join();
//        	
//        	return (Math.max(leftLong, rightLong));
//        	
//        }
//    	
//    }
//    private static ForkJoinPool POOL = new ForkJoinPool();
//    
//    public static int forkLongestTask(int[] arr){
//    	return POOL.invoke(new longestTask(arr, 0, arr.length));
//    }
//    
//    private static void usage() {
//        System.err.println("USAGE: LongestSequence <number> <array> <sequential cutoff>");
//        System.exit(2);
//    }
//    public static void main(String[] args) {
//        if (args.length != 3) {
//            usage();
//        }
//        int val = 0;
//        int[] arr = null;
//        try {
//            val = Integer.parseInt(args[0]); 
//            String[] stringArr = args[1].replaceAll("\\s*",  "").split(",");
//            arr = new int[stringArr.length];
//            for (int i = 0; i < stringArr.length; i++) {
//                arr[i] = Integer.parseInt(stringArr[i]);
//            }
//            System.out.println(getLongestSequence(val, arr, Integer.parseInt(args[2])));
//        } catch (NumberFormatException e) {
//            usage();
//        }
//    }
//}


