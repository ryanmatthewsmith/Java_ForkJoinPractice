package getLeftMostIndex;

public class LeftIndexHelper {

    public int foundIndex;
    public boolean fellOff;
    public int fellOffIndex;
    public int fellOffGap;

    public LeftIndexHelper(int foundIndex, boolean fellOff, int fellOffIndex, int fellOffGap) {
    	this.foundIndex = foundIndex;
        this.fellOff = fellOff;
        this.fellOffIndex = fellOffIndex;
        this.fellOffGap = fellOffGap;
    }       
}
