package getLeftMostIndex;

import java.util.concurrent.ForkJoinPool;
import java.util.concurrent.RecursiveTask;

public class GetLeftMostIndex {
	public static int CUTOFF;
	public static char[] NEEDLEIN;
    public static int getLeftMostIndex(char[] needle, char[] haystack, int sequentialCutoff) {
        NEEDLEIN = needle;
        CUTOFF = sequentialCutoff;
        LeftIndexHelper first = getLeftInvoke(haystack);
        return first.foundIndex;
    }
    
    public static LeftIndexHelper seqGetLeftMostIndex(char[] haystack, int lo, int hi){
    
    	LeftIndexHelper curr = new LeftIndexHelper(-1, false, -1, 0);
    	boolean first = false;
    	if (hi > haystack.length || lo < 0) return curr;
    	int i = lo;
    	while (i<hi){
    		if(NEEDLEIN[0] == haystack[i]){
    			curr.foundIndex = i;
    			int needleIndex = 1;
    			i++;
    			while(needleIndex < NEEDLEIN.length && i<hi && NEEDLEIN[needleIndex] == haystack[i]){
    				i++;
    				needleIndex++;    				
    			}

    		
    			if (needleIndex == NEEDLEIN.length){
    				curr.fellOff = false;
    				return curr;
    			} 
                if (i>=hi && NEEDLEIN.length > needleIndex && !first){
                	first = true;
    				curr.fellOff = true;
    				curr.fellOffGap = i-curr.foundIndex-1;
    				curr.fellOffIndex = curr.foundIndex;
    			}
    			i = curr.foundIndex;
    			curr.foundIndex = -1;
    			
    		}
    		i++;
    	}
    	return curr;
    }
    
    static class leftIndexTask extends RecursiveTask<LeftIndexHelper>{
    	char[] haystack;
    	int lo;
    	int hi;
    	
    	public leftIndexTask(char[] haystack, int lo, int hi){
    		this.haystack = haystack;
    		this.lo = lo;
    		this.hi = hi;   		
    	}
    	public LeftIndexHelper compute(){
        	if(hi-lo <= CUTOFF){
        		return (seqGetLeftMostIndex(haystack, lo, hi));
        	}
        	
        	int mid = lo + (hi-lo)/2;
        	
        	leftIndexTask left = new leftIndexTask(haystack, lo, mid);
        	leftIndexTask right = new leftIndexTask(haystack, mid, hi);
        	
        	left.fork();
        	LeftIndexHelper rightIn = right.compute();
        	LeftIndexHelper leftIn = left.join();
        	LeftIndexHelper border;
        	
        	if(leftIn.foundIndex != -1 && rightIn.foundIndex != -1){
        		if (leftIn.foundIndex<=rightIn.foundIndex)return leftIn;
        		else return rightIn;
        	}
        	
        	if(leftIn.foundIndex != -1)return leftIn; 
            
        	if (leftIn.fellOff){
        		border = seqGetLeftMostIndex(haystack, leftIn.fellOffIndex, leftIn.fellOffIndex+NEEDLEIN.length+leftIn.fellOffGap);
        		if(border.foundIndex != -1){
        			return border;
        		}else{
        			leftIn.fellOff = false;
        		}
        	}
            if (rightIn.foundIndex != -1)return rightIn;
        	
            if (rightIn.fellOff){
        		border = seqGetLeftMostIndex(haystack, rightIn.fellOffIndex, rightIn.fellOffIndex+NEEDLEIN.length+rightIn.fellOffGap);
        		if(border.foundIndex != -1){
        			return border;
        		} else {
        			rightIn.fellOff = false;
        		}
        	} 
        	return rightIn;  
    	}
    }
    private static ForkJoinPool POOL = new ForkJoinPool();
    
    public static LeftIndexHelper getLeftInvoke(char[] haystack){
  	  return POOL.invoke(new leftIndexTask(haystack, 0, haystack.length));
    }

    private static void usage() {
        System.err.println("USAGE: GetLeftMostIndex <needle> <haystack> <sequential cutoff>");
        System.exit(2);
    }

    public static void main(String[] args) {
        if (args.length != 3) {
            usage();
        }

        char[] needle = args[0].toCharArray();
        char[] haystack = args[1].toCharArray();
        try {
            System.out.println(getLeftMostIndex(needle, haystack, Integer.parseInt(args[2])));
        } catch (NumberFormatException e) {
            usage();
        }
    }
}
